# reference-vars-merge-failure

## About
This repo demonstrates a bug in the gitlab-ci `!reference` tag usage.  Issue: [gitlab-org/gitlab#407268](https://gitlab.com/gitlab-org/gitlab/-/issues/407268)

Given we are using the `!reference` tag to import shared config variables from another job/file, when the target of the import is a job extension, then the imported variables replace the parent job's variables, instead of merging them.

To view the symptoms of this behavior, examine the simple example `.gitlab-ci.yml` file and its included `.shared.config` file included here.  Then navigate to `CI/CD → Editor` to compare the merged job YAML `variables` for the two extended jobs.  Note the differences when using `!reference` versus inline literals.

[GitLab !reference Tag Documentation](https://docs.gitlab.com/ee/ci/yaml/yaml_optimization.html#reference-tags)

*.shared.config.gitlab-ci.yml*
```
---
.common-config:
  common-vars:
    VAR_3: banannaBread
    VAR_4: carrotCake
```

*.gitlab-ci.yml*
```
---
include:
  - local: ".shared.config.gitlab-ci.yml"

.parent-job:
  variables:
    VAR_1: appleSauce
    VAR_2: orangeJuice

# !reference common variables
child-job-a:
  extends: .parent-job
  variables: !reference [.common-config, common-vars]
  script: echo "Child Job B"

# Inlines common variables
child-job-b:
  extends: .parent-job
  variables:
    VAR_3: banannaBread
    VAR_4: carrotCake
  script: echo "Child Job A"
```

## Expected Result
Both child jobs should merge the variables lists
```
child-job-a:
  variables:
    VAR_1: appleSauce
    VAR_2: orangeJuice
    VAR_3: banannaBread
    VAR_4: carrotCake# 
child-job-b:
  variables:
    VAR_1: appleSauce
    VAR_2: orangeJuice
    VAR_3: banannaBread
    VAR_4: carrotCake
```

## Actual Result
Job A only contains the referenced variables instead of merging the lists.

![image Incorrectly merged YAML](merged-yaml.png)
